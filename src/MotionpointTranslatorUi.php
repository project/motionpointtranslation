<?php

namespace Drupal\tmgmt_motionpoint;

use Drupal;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator;
use Exception;

/**
 * Motionpoint translator UI.
 */
class MotionpointTranslatorUi extends TranslatorPluginUiBase {

  /**
   * Constants
   */
  const MP_JOB_STATUS_QUEUED = 'QUEUED';
  const MP_JOB_STATUS_COMPLETED = 'COMPLETED';
  const MP_JOB_STATUS_APPROVED = 'APPROVED';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    $api_base_url = UrlHelper::isValid($translator->getSetting('motionpoint_api_url')) ? $translator->getSetting('motionpoint_api_url') : 'https://api.motionpoint.com';

    $form['motionpoint_api_url'] = [
      '#type' => 'textfield',
      '#title' => t('MotionPoint API URL'),
      '#default_value' => $api_base_url,
      '#description' => t('Please enter MotionPoint API base URL.'),
      '#required' => TRUE,
    ];
    $form['motionpoint_username'] = [
      '#type' => 'textfield',
      '#title' => t('Your Username'),
      '#description' => t('Please enter username'),
      '#required' => TRUE,
    ];
    $form['motionpoint_apikey'] = [
      '#type' => 'textfield',
      '#title' => t('API Key'),
      '#description' => t('Please enter API-Key'),
      '#required' => TRUE,
      '#maxlength' => 256, // default length is 128; set this to accept long apikey
    ];
    $form['motionpoint_client_id'] = [
      '#type' => 'textfield',
      '#title' => t('MotionPoint API ID'),
      '#description' => t('Please enter MotionPoint API ID'),
      '#required' => TRUE,
    ];
    $form['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => t('Use Sandbox'),
      '#default_value' => $translator->getSetting('use_sandbox'),
      '#description' => t('Check to use MotionPoint Sandbox environment.'),
      '#required' => FALSE,
    ];
    $form['submit_setting'] = [
      '#type' => 'radios',
      '#title' => t('Default Submit Setting'),
      '#required' => true,
      '#default_value' => $translator->getSetting('submit_setting'),
      '#description' => t('Default Submit Setting of the project selected in job checkout.'),
      '#options' => $this->getSubmitSetting(),
    ];

    $form += parent::addConnectButton();

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $isValidated = false;

    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $translator_plugin */
    $translator_plugin = $translator->getPlugin();
    $translator_plugin->setTranslator($translator);
  
    $username = $translator->getSetting('motionpoint_username');
    $client_id = $translator->getSetting('motionpoint_client_id');

    if ($username == trim($username) && strpos($username, ' ') !== false) {
      $form_state->setErrorByName('settings][motionpoint_username', t('Username should not contain space. '));
      return;
    }

    if (!is_numeric($client_id)) {
      $form_state->setErrorByName('settings][motionpoint_client_id', t('MotionPoint API ID should be numbers only.'));
      return;
    }

    $response = $translator_plugin->sendRequest('/queues', 'GET', [], NULL, NULL);
    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {
      $isValidated = true;
    }

    if ( !$isValidated ) {
      $form_state->setErrorByName('settings][motionpoint_aipkey', t('Authorization denied. Please contact support@motionpoint.com.'));
    }
  }


  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $settings, FormStateInterface $form_state, JobInterface $job) {

    if ($job->isContinuous()) {
      $settings['continuous_info'] = array(
        '#type' => 'fieldset',
        '#markup' => t('Please see <b>Job Items</b> view for translation processed via this continuous job.'),
      );
    } else { 
      
      /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $translator_plugin */
      $translator_plugin = $this->getTranslatorPluginForJob($job);

      try{
        $quote = $translator_plugin->getStatistics($job);
      } catch(Exception $e) {
        \Drupal::logger('tmgmt_moitionpoint')->error($e->getMessage());
      }

      $settings['quote'] = array(
        '#type' => 'fieldset',
        '#title' => t('Translation Statistics'),
      );

      if (!$quote) {   
        $settings['quote']['error'] = array(
          '#type' => 'item',
          '#markup' => t('Statistics cannot be calculated. Please try again in a few minutes. If situation persists please contact support@motionpoint.com.'),
        );        
        return $settings;
      }

      if (array_key_exists('language_not_support', $quote)) {
        $settings['quote']['language_not_support'] = array(
          '#type' => 'item',
          '#markup' => t('The target language is not supported. Please contact support@motionpoint.com.'),
        );  
        return $settings;
      }

      $settings['quote']['words_not_translated'] = array(
        '#type' => 'item',
        '#title' => t('Untranslated words'),
        '#markup' => $quote['words_not_translated']
      );

      $settings['quote']['words_total'] = array(
        '#type' => 'item',
        '#title' => t('Total words'),
        '#markup' => $quote['words_total'],
        '#description' => t('<br>Please note that MotionPoint provides the unique translatable word count, which may differ from the Drupal word count.'),
      );

      $translator = $job->getTranslator();
      $settings['submit_setting'] = [
        '#type' => 'radios',
        '#title' => t('Submit Setting'),
        /* Check if the setting was already configured, otherwise set it to single/0 */
        '#default_value' => !is_null($projectMode = $translator->getSetting('submit_setting'))
            ? $projectMode : 0,
        '#options'       => $this->getSubmitSetting(),
      ];
    }

    return $settings;
  }


  /**
   * {@inheritdoc}
   */
  public function checkoutInfo(JobInterface $job) {

    $form = [];

    /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $translator_plugin */
    $translator_plugin = $this->getTranslatorPluginForJob($job);

    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $job_reference = $job->getReference();
    $api_job_id = explode("-", $job_reference, 2)[0];
    $pulled_translation = false;  

    if (!is_null($projectMode = $job->getSetting('submit_setting')) && $projectMode == 1) {
      
      foreach ($job->getRemoteMappings() as $remote) {
        $job_item = $remote->getJobItem();
        $api_job_id = $remote->getRemoteIdentifier2();
        
        if ($job_item->isActive() && $job_item->getCountPending() > 0) {

          $api_job_status = $translator_plugin->getApiJobStatus($api_job_id, $source_language, $target_language);

          if ($api_job_status == static::MP_JOB_STATUS_COMPLETED) {
            $pulled_translation = $translator_plugin->updateTranslationsToJob($job, $translator_plugin, $api_job_id);
          } 
        } 
      }

      if ($pulled_translation) {
        \Drupal::messenger()->addMessage(t("Translation has been received for some items.", ));
      }

      $form['motionpoint_status'] = [
        '#markup' => t("This job is submited in Multiple Jobs mode. Please check <b>Job Items</b> list for already translated items."),
        '#weight' => -10,
      ];
    
    } else {
      
      if ($job->isActive() && $job->getCountPending() > 0) {
        $api_job_status = $translator_plugin->getApiJobStatus($api_job_id, $source_language, $target_language);
        
        if ($api_job_status == static::MP_JOB_STATUS_COMPLETED) {
          $job->reference = $job_reference . '-' . static::MP_JOB_STATUS_COMPLETED;
          $job->save();
          $pulled_translation = $translator_plugin->updateTranslationsToJob($job, $translator_plugin, $api_job_id);
          
          if ($pulled_translation) {
            \Drupal::messenger()->addMessage(t('Translation has been received for MotionPoint job id @id', ['@id' => $api_job_id]));
          }

        } else {
          $form['motionpoint_status'] = [
            '#markup' => t("Job is in progress."),
            '#weight' => -10,
          ];          
        }
      } 
    }
    
    if ($job->isActive() && $job->getCountPending() == 0 && ($job->getCountTranslated() > 0 || $job->getCountAccepted() > 0)) {
      $form['motionpoint_status'] = [
        '#markup' => t("Translation completed by MotionPoint."),
        '#weight' => -10,
      ];
    }

    if ($job->isFinished()) {
      $form['motionpoint_status'] = [
        '#markup' => t("This job is already finished."),
        '#weight' => -10,
      ];
    }

    return $form;
  }


  /** 
   * Get translator_plugin for a job.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   TMGMT Job Entity.
   *
   * @return \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator
   *   Motionpoint Translator plugin.
   */
  public function getTranslatorPluginForJob(JobInterface $job) {
    /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $plugin */
    $translator_plugin = $job->getTranslator()->getPlugin();
    $translator_plugin->setTranslator($job->getTranslator());
    return $translator_plugin;
  }


  /**
   * Available submit settings.
   *
   * @var array
   */
  private $submitSettings = [
    0 => 'Single Job - all pages are combined into one MotionPoint job',
    1 => 'Multiple Jobs - an individual MotionPoint job is created for each page',
  ];

  /**
   * Return available submit settings.
   *
   * @return array
   */
  public function getSubmitSetting()
  {
    $out = [];
    foreach ($this->submitSettings as $value) {
        $out[] = t($value);
    }

    return $out;
  }
}