<?php

namespace Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\Entity\RemoteMapping;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Error;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Motionpoint translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "motionpoint",
 *   label = @Translation("MotionPoint"),
 *   description = @Translation("MotionPoint translation service."),
 *   logo = "icons/motionpoint_thumbnail.png",
 *   ui = "Drupal\tmgmt_motionpoint\MotionpointTranslatorUi",
 * )
 */
class MotionpointTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {

  /**
   * Constants
   */
  const JOB_CONTENT_TYPE = 'application/xliff+xml';
  const JOB_COMMENT = 'This job is from Drupal connector';
  const JOB_REFERENCE_ID = 'drupal job and item id';
  const MP_WORDS_TRANSLATED = 'words-translated=';
  const MP_WORDS_NOT_TRANSLATED = 'words-not-translated=';
  const MP_WORDS_SUPPRESSED = 'words-suppressed=';
  const MP_WORDS_NOT_QUEUEABLE = 'words-not-queueable=';
  const MP_JOB_STATUS_QUEUED = 'QUEUED';
  const MP_JOB_STATUS_COMPLETED = 'COMPLETED';
  const MP_SANDBOX_URL = "https://sandboxapi.motionpoint.com";
  const MP_SANDBOX_CLIENT_ID = 1;
  const MP_SANDBOX_SRC_LANG = 'en';
  const MP_SANDBOX_DESTINATION_LANG = 'es';

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * TMGMT translator.
   *
   * @var \Drupal\tmgmt\TranslatorInterface
   */
  protected $translator;


  /**
   * Constructs a MotionpointTranslator object.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Guzzle HTTP client.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(ClientInterface $client, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * Sets a Translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   * 
   */
  public function setTranslator(TranslatorInterface $translator) {
    $this->translator = $translator;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \GuzzleHttp\ClientInterface $client */
    $client = $container->get('http_client');
    return new static(
      $client,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }


  /**
   * {@inheritdoc}
  */
  public function defaultSettings() {
    $defaults = parent::defaultSettings();
    $defaults['export_format'] = 'xlf';
    $defaults['xliff_cdata'] = TRUE;
    return $defaults;
  }


  /**
   * 
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($translator->getSetting('motionpoint_apikey')) {
      return AvailableResult::yes();
    }

    return AvailableResult::no(t('@translator is not available. Please make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }


 /**
   * Get number of words will be translated if submitted
   *
   * @param JobInterface $job
   *
   * @return array|bool
   *   Returns the statistics info, FALSE otherwise.
   */
  public function getStatistics(JobInterface $job) {

    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $job_id = $job->id();
 
    /** @var \Drupal\tmgmt_file\Format\FormatInterface $xliff_converter */
    $xliff_converter = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance('xlf');
    $xliff_content = $xliff_converter->export($job);
    $file_name = "mpdrupaljobid_{$job_id}_{$source_language}_{$target_language}.xlf";

    $options['multipart'] = [
      [
        'name' => 'transactionReferenceId',
        'contents' => static::JOB_REFERENCE_ID . ' ' . $job_id, 
      ],
      [
        'name' => 'comments',
        'contents' => static::JOB_COMMENT,
      ],
      [
        'name' => 'contentType',
        'contents' => static::JOB_CONTENT_TYPE,
      ],
      [
        'name' => 'contentCharset',
        'contents' => 'utf-8',
      ],
      [
        'name' => 'file',
        'contents' => $xliff_content,
        'filename' => $file_name,
      ],
      [
        'name' => 'queueUntranslatedContent',
        'contents' => 'false',
      ]
    ];
 
    $response = $this->sendRequest('/translations', 'POST', $options, $source_language, $target_language);

    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {

      $mpstats = $response->getHeaders()['X-MPTrans-Statistics'][0];

      $quote = $this->calculateStatistics($mpstats);
      return $quote;
    } elseif (method_exists($response, 'getCode') && $response->getCode() == 404) {
      $quote['language_not_support'] = true;
      return $quote;
    } else {
      return false;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {

    if (!is_null($projectMode = $job->getSetting('submit_setting')) && $projectMode == 1) { 
      $this->requestJobItemsTranslation($job->getItems());
      return;
    }

    $this->setTranslator($job->getTranslator());
    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $job_id = $job->id();

    /** @var \Drupal\tmgmt_file\Format\FormatInterface $xliff_converter */
    $xliff_converter = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance('xlf');
    $xliff_content = $xliff_converter->export($job);
  
    $callbackurl = Url::fromRoute('tmgmt_motionpoint.job_callback', ['tmgmt_job' => $job_id,])->setAbsolute()->toString();

    $api_job_id = $this->createApiJob($job_id, $source_language, $target_language, $xliff_content, $callbackurl);
    if ($api_job_id) {
      $job->submitted('Created a new job in MotionPoint @source->@target with MotionPoint job id: @id', ['@id' => $api_job_id,'@source' => strtoupper($source_language), '@target' => strtoupper($target_language)], 'status');
      if ((int) $job->getReference() < 1) {  // job reference has not been set before
        $job->reference = $api_job_id; 
        $job->save();
      }
    } 
  }


  /**
   * Get translation job status
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   * 
   * @return string
   *   Returns the api job status
   */
  public function getApiJobStatus($id, $source_language, $target_language) {

    $api_job_status = 'UNKNOWN';
    $path = '/translationjobstats/jobs' . '/' . $id;
    $response = $this->sendRequest($path, 'POST', [], $source_language, $target_language);

    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {
      $data = json_decode($response->getBody()->getContents());
      $api_job_status = $data->status;

      return $api_job_status; 
    } 

    return $api_job_status;
  }


  /**
   * Get translations for API job of a given drupal job.
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   * @return
   * Translated data array.
   */
  public function getTranslations($id, $source_language, $target_language) {

    $path = '/translations/jobs' . '/' . $id;
    
    $response = $this->sendRequest($path, 'POST', [], $source_language, $target_language);
    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {

      $data = $response->getBody()->getContents(); //  get response body as string then format as xliff 

      /** @var \Drupal\tmgmt_file\Format\FormatInterface $xliff_converter */
      $xliff_converter = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance('xlf');
      $received_translation = $xliff_converter->import($data, FALSE);
    } 
    return $received_translation;
  }


  /**
   *  Fetch translation and update job after received translation.
   * 
   *  @return bool
   *    Returns TRUE if success, otherwise FALSE
   */
  public function updateTranslationsToJob($job, $translator_plugin, $api_job_id) {
    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();

    try {
      $received_translation = $translator_plugin->getTranslations($api_job_id, $source_language, $target_language);
      // Xliff that could not be parsed will return a boolean and not show a proper error
      if (is_bool($received_translation) === true) { throw new Exception(); };
      $job->addTranslatedData($received_translation, [], TMGMT_DATA_ITEM_STATE_TRANSLATED);
      $job->addMessage('Translation has been received for MotionPoint job id @id', ['@id' => $api_job_id]);
      $translator_plugin->approveApiJob($api_job_id, $source_language, $target_language);
      return true;
    } catch (Exception $e) {
      \Drupal::logger('tmgmt_motionpoint')->warning('Translation cannot be retrieved. If situation persists please contact support@motionpoint.com.' );
    }
    return false;
  } 

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $remote_languages = [];
    return $remote_languages;
  }


  /**
   * Create API job when user requests translation
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   * @param mixed $xliff_content
   * @param string $callbackurl
   *  
   * @return int|bool
   *  Returns API job id if job is created, otherwise FALSE
   * 
   */
  public function createApiJob($id, $source_language, $target_language, $xliff_content, $callbackurl) {

    $file_name = "mpdrupaljobid_{$id}_{$source_language}_{$target_language}.xlf";

    $options['multipart'] = [
      [
        'name' => 'transactionReferenceId',
        'contents' => static::JOB_REFERENCE_ID . ' ' . $id,
      ],
      [
        'name' => 'comments',
        'contents' => static::JOB_COMMENT,
      ],
      [
        'name' => 'contentType',
        'contents' => 'application/x-xliff+xml',
      ],
      [
        'name' => 'contentCharset',
        'contents' => 'utf-8',
      ],
      [
        'name' => 'callbackUrl',
        'contents' => $callbackurl,
      ],
      [
        'name' => 'file',
        'contents' => $xliff_content,
        'filename' => $file_name,
      ]
    ];

    $response = $this->sendRequest('/translationjobs', 'POST', $options, $source_language, $target_language); 
    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {
      $data = json_decode($response->getBody()->getContents());
      return $data->id;
    } else {
      $this->messenger()->addWarning($this->t('Unable to send job to MotionPoint @source->@target', ['@source' => strtoupper($source_language), '@target' => strtoupper($target_language)]));
      return false;
    }
  }


  /**
   * Set API job to approved
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   */
  public function approveApiJob($id, $source_language, $target_language) {

    $path = '/translationjobs/status';  
    $payload = array(
      'jobId' => $id,
      'newStatusId'    => '7',
    );
    $options['body'] = json_encode($payload);
    $this->sendRequest($path, 'POST', $options, $source_language, $target_language);

  }


  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    $this->setTranslator($job->getTranslator());

    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $job_reference = (string)$job->getReference();
    $api_job_id = explode("-", $job_reference, 2)[0];

    if (!is_null($projectMode = $job->getSetting('submit_setting')) && $projectMode == 1) {
      
      $count_items = count($job->getRemoteMappings());
      $count_cancelled = 0;
      $cancelled_ids = '';

      foreach ($job->getRemoteMappings() as $remote) {
        $job_item = $remote->getJobItem();
        $api_job_id = $remote->getRemoteIdentifier2();
        if ($job_item->isActive()) {
          $cancel_result = $this->cancelApiJob($api_job_id, $source_language, $target_language);
          if ($cancel_result) {
            $job_item->abortTranslation();
            $cancelled_ids = $cancelled_ids . ' ' . $api_job_id;
            $count_cancelled++;
          } 
        } elseif ($job_item->isAborted()) {
          $count_cancelled++;
        }
      }

      if ($count_cancelled == 0 || $cancelled_ids == '') {
        $this->messenger()->addWarning($this->t('Cannot abort this job. All items in this job are being processed or have been aborted already.'));
      } else {
        if ($count_cancelled == $count_items) {
          $job->aborted('All job items have been aborted.');
        } else {
          $job->addMessage('Following MotionPoint job ids have been aborted: @cancelled_ids. Others are being processed or have been aborted already.', ['@cancelled_ids' => $cancelled_ids]);
        }
      }

    } else {
      if (strpos($job_reference, static::MP_JOB_STATUS_COMPLETED) !== false) {
        $this->messenger()->addWarning($this->t('Translation is being processed. This job can no longer be aborted.'));
      } else {
        $cancel_result = $this->cancelApiJob($api_job_id, $source_language, $target_language);
        if ($cancel_result) {
          $job->aborted('This job has been aborted. MotionPoint will not process the content requested in this job.');
        } else {
          $this->messenger()->addWarning($this->t('Translation is being processed. This job can no longer be aborted.'));
        }
      }
    }
  }


  /**
   * Cancel API job when user aborts job
   *
   * @param int $id
   * @param string $source_language
   * @param string $target_language
   *  
   * @return bool
   *   TRUE if the job is cancelled, FALSE otherwise.
   */
  public function cancelApiJob($id, $source_language, $target_language) {

    $path = '/translationjobs/status';  
    $payload = array(
      'jobId' => $id,
      'newStatusId'    => '6',
    );
    $options['body'] = json_encode($payload);
    $response = $this->sendRequest($path, 'POST', $options, $source_language, $target_language);

    if (method_exists($response, 'getStatusCode') && $response->getStatusCode() == 200) {
      return true;
    } elseif (method_exists($response, 'getCode') && $response->getCode() == 409) {
      return false;
    }
  }


  /**
   * Does a request to MotionPoint API.
   *
   * @param string $path
   *   Resource path.
   * @param string $method
   *   (Optional) HTTP method (GET, POST...). By default uses GET method.
   * @param array $params
   *   (Optional) Additional Form parameters to send.
   * @param string $source_language
   *   (Optional) Source language when request translation.
   * @param array $target_language
   *   (Optional) Target language when request translation.
   * @return object|int
   *   Response object or status code.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   */
  public function sendRequest($path, $method = 'GET', $params = [], $source_language = NULL, $target_language = NULL) {
    
    if (!$this->translator) {
      throw new TMGMTException('There is no Translator entity. Unable to access to the MotionPoint API.');
    }
    
    $sandbox = !is_null($sandbox = $this->translator->getSetting('use_sandbox'))? $sandbox: 0;
    
    if ($sandbox == 1) {
        $url = STATIC::MP_SANDBOX_URL . $path;
        $username = $this->translator->getSetting('motionpoint_username');
        $apikey = $this->translator->getSetting('motionpoint_apikey');
        $client_id = STATIC::MP_SANDBOX_CLIENT_ID;
        $source_language = STATIC::MP_SANDBOX_SRC_LANG;
        $target_language = STATIC::MP_SANDBOX_DESTINATION_LANG;
    } else {
        $url = $this->translator->getSetting('motionpoint_api_url') . $path;
        $username = $this->translator->getSetting('motionpoint_username');
        $apikey = $this->translator->getSetting('motionpoint_apikey');
        $client_id = $this->translator->getSetting('motionpoint_client_id');
    }

    $options = [];

    if ($path == '/queues') {  
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
      ];
    } elseif ($path == '/translationjobs' || $path == '/translations') {   
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
        'X-MotionCore-Queue' => $source_language . '.' . $target_language . '.' . $client_id,
      ];
      $options += $params;
    } else {
      $options['headers'] = [
        'Authorization' => $apikey,
        'X-MotionCore-UserName' => $username,
        'X-MotionCore-Queue' => $source_language . '.' . $target_language . '.' . $client_id,
        'Content-Type' => 'application/json',
      ];
      $options += $params;
    }


    try {
      $response = $this->client->request($method, $url, $options);
      return $response;
    }
    catch (Exception $e) {
      if (method_exists($e, 'getCode')) {
        \Drupal::logger('tmgmt_motionpoint')->warning('API Request Exception, status code is: ' . print_r($e->getCode(), TRUE) . '</code></pre>' );
      }
      return $e;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function requestJobItemsTranslation(array $job_items) {
    
    $job = reset($job_items)->getJob();    
    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();
    $job_id = $job->id();
    $api_job_ids = '';
    $this->setTranslator($job->getTranslator());
  
    foreach ($job_items as $job_item) {

        if ($job->isContinuous()) {
          $job_item->active();
        }

        $job_item_id = $job_item->id();
        $unique_job_id = $job_id . '-' .$job_item_id;
        // Xliff method will split combined xliff appropriately
        $xliff_content = $this->getXliffData($job, [$job_item]);

        $callbackurl = Url::fromRoute('tmgmt_motionpoint.job_item_callback', ['tmgmt_job_item' => $job_item_id,])->setAbsolute()->toString();

        $api_job_id = $this->createApiJob($unique_job_id, $source_language, $target_language, $xliff_content, $callbackurl);
        if ($api_job_id) {
          $job_item->addRemoteMapping(NULL, $unique_job_id, ['remote_identifier_2' => $api_job_id]);
          $api_job_ids= $api_job_ids . ' ' . $api_job_id;
        } else {
          return;
        }
    }
     
    $job->submitted('Created new jobs in MotionPoint @source->@target with MotionPoint job ids: @id', ['@id' => $api_job_ids, '@source' => strtoupper($source_language), '@target' => strtoupper($target_language)], 'status');
    $job->reference = 'MULTI';
    $job->save();  
  }
  

  /**
  * Returns the XLIFF data for a JobInterface based on job items given.
  *
  * @param \Drupal\tmgmt\JobInterface $job
  *   The translation job.
  * @param \Drupal\tmgmt\JobItemInterface[] $job_items
  *   Limit the export to the provided job items.
  *
  * @return array
  */
  public function getXliffData(JobInterface $job, array $job_items) {
    // Ensure the job item list is keyed by the job item ID.
    $job_item_ids = [];
    // Only one item in the array so only a portion of the XLIFF content is taken
    foreach ($job_items as $job_item) {
      $job_item_ids[$job_item->id()] = $job_item;
    }
 
    $conditions['tjiid'] = ['value' => array_keys($job_item_ids), 'operator' => 'IN'];

    $data = \Drupal::service('plugin.manager.tmgmt_file.format')->createInstance('xlf')->export($job, $conditions);
    
    return $data;
  }


  /**
  * Calculates statistics
  *
  * @param string mpstats
  *   The translation job.
  *
  * @return array
  *   Returns calculated statistics info
  */
  public function calculateStatistics(string $mpstats) {
    
    $start_translated = strpos($mpstats, static::MP_WORDS_TRANSLATED) + strlen(static::MP_WORDS_TRANSLATED);
    $end_translated = strpos($mpstats, static::MP_WORDS_NOT_TRANSLATED) - $start_translated -1;
    $words_translated = substr($mpstats ,$start_translated, $end_translated);
    
    $start_not_translated = strpos($mpstats, static::MP_WORDS_NOT_TRANSLATED) + strlen(static::MP_WORDS_NOT_TRANSLATED);
    $end_not_translated = strpos($mpstats, static::MP_WORDS_SUPPRESSED) - $start_not_translated -1;
    $words_not_translated = substr($mpstats ,$start_not_translated, $end_not_translated);

    $start_suppressed = strpos($mpstats, static::MP_WORDS_SUPPRESSED) + strlen(static::MP_WORDS_SUPPRESSED);
    $end_suppressed = strpos($mpstats, static::MP_WORDS_NOT_QUEUEABLE) - $start_suppressed -1;
    $words_suppressed = substr($mpstats ,$start_suppressed, $end_suppressed);
    
    $words_total = $words_translated + $words_not_translated + $words_suppressed; 

    $quote['words_not_translated'] = $words_not_translated;
    $quote['words_total'] = $words_total;

   return $quote;
 }
  
}
