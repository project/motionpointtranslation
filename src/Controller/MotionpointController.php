<?php

namespace Drupal\tmgmt_motionpoint\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Exception;
use Error;

/**
 * Handles callbacks for tmgmt_motionpoint module.
 */
class MotionpointController extends ControllerBase
{ 

  /**
   * Constants
   */
  const MP_CALLBACK_STATUS_COMPLETE = 'TRANSLATION_COMPLETED';


  /**
   * Callback function for Job
   *
   * @param \Drupal\tmgmt\JobInterface $tmgmt_job
   *   Translation job.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The callback request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function jobCallback(JobInterface $tmgmt_job, Request $request) { 

    if (!empty($request->getContent())) {
      $request_body = json_decode($request->getContent());
      $api_job_id = $request_body->jobId;
      $api_job_status = $request_body->eventType;

      if ($tmgmt_job->isActive() && $api_job_status == static::MP_CALLBACK_STATUS_COMPLETE) {

        /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $plugin */
        $translator_plugin = $tmgmt_job->getTranslator()->getPlugin();
        $translator_plugin->setTranslator($tmgmt_job->getTranslator());
        $result = $translator_plugin->updateTranslationsToJob($tmgmt_job, $translator_plugin, $api_job_id);
        if ($result) {
          return new Response("Translation has been successfully updated to drupal job.", Response::HTTP_OK);
        } else {
          return new Response("Failed to update translation to drupal job.", Response::HTTP_INTERNAL_SERVER_ERROR);
        }
      } elseif (!$tmgmt_job->isActive()) {
        return new Response("Drupal job is no longer active.", Response::HTTP_OK);
      } else {
        return new Response("Api job status is not COMPLETED yet.", Response::HTTP_OK);
      }
      
    } else {
      return new Response("Request body is empty.", Response::HTTP_OK);
    }
  }


  /**
   * Callback function for Job item
   *
   * @param \Drupal\tmgmt\JobItemInterface $tmgmt_job_item
   *   Translation job.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The callback request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function jobItemCallback(JobItemInterface $tmgmt_job_item, Request $request) { 
    
    if (!empty($request->getContent())) {
      $request_body = json_decode($request->getContent());
      $api_job_id = $request_body->jobId;
      $api_job_status = $request_body->eventType;

      if ($tmgmt_job_item->isActive() && $api_job_status == static::MP_CALLBACK_STATUS_COMPLETE) {
        $job = $tmgmt_job_item->getJob();
      
        /** @var \Drupal\tmgmt_motionpoint\Plugin\tmgmt\Translator\MotionpointTranslator $plugin */
        $translator_plugin = $job->getTranslator()->getPlugin();
        $translator_plugin->setTranslator($job->getTranslator());
        $result = $translator_plugin->updateTranslationsToJob($job, $translator_plugin, $api_job_id);
        if ($result) {
          return new Response("Translation has been successfully updated to drupal job item.", Response::HTTP_OK);
        } else {
          return new Response("Failed to update translation to drupal job item.", Response::HTTP_INTERNAL_SERVER_ERROR);
        } 
      } elseif (!$tmgmt_job_item->isActive()) {
        return new Response("Drupal job item is no longer active.", Response::HTTP_OK);
      } else {
        return new Response("Api job status is not COMPLETED yet.", Response::HTTP_OK);
      }

    } else {
      return new Response("Request body is empty.", Response::HTTP_OK);
    }
  }

}
